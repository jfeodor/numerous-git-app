import numpy
import pandas
from numerous.client import NumerousClient, ScenarioStatus


def entrypoint(client: NumerousClient, components):
    client.set_scenario_progress('Writing', ScenarioStatus.RUNNING)
    client.set_timeseries_meta_data([{'name': 'h'}])
    h0 = components['bouncy']['parameters']['h0']
    G = components['bouncy']['parameters']['G']

    for t in range(10):
        client.writer.write_row({'_index': t, 'h': bounce(h0, G, t)})

    with open('report.html', 'w') as fp:
        fp.write(f"<html><body><h1>Report</h1><p>Report!</p></body></html>")
    client.upload_file('report.html', 'report')


def bounce(h0: float, G: float, t: float):
    h = h0 + -G * t
    return max(0, h)